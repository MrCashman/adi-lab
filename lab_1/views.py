from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Ihsan Adi Pratama' # TODO Implement this
born_place = 'Bandar Lampung'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 6, 18) #TODO Implement this, format (Year, Month, Date)
npm = 2101909908 # TODO Implement this
mhs_univ = 'Bina Nusantara'
sentence = 'The reason why i joined this online programming class is because i want to know more ' \
           'people who have the same passion as mine and i also want to improve my skill by gaining a ' \
           'lot of new knowledge in these study group'
# Create your views here.
def index(request):

    response = {'name': mhs_name, 'place': born_place, 'age': calculate_age(birth_date.year), 'npm': npm,
                'univ': mhs_univ, 'sentence': sentence}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
